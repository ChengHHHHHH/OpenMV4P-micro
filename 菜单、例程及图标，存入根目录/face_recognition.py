'''
OpenMV4P_micro
面部识别

面部识别例程，使用自带的神经网络模型寻找面部，通过模板匹配录入并比对。
识别准确率不可靠，仅供学习参考。

使用说明：
按右键录入人脸，按左键取消录入信息，可重新录入。

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20220909
'''
import sensor, image, time,screen,button
from os import mkdir

sensor.reset()
sensor.set_framesize(sensor.QVGA)
sensor.set_windowing(320,172)
sensor.set_pixformat(sensor.RGB565)
sensor.set_vflip(True)
sensor.set_hmirror(True)

clock = time.clock()

screen=screen.screen()

face_cascade = image.HaarCascade("frontalface", stages=25)

img_grayscale = sensor.alloc_extra_fb(320,172, sensor.GRAYSCALE)
face = sensor.alloc_extra_fb(50,50, sensor.GRAYSCALE)
face_size=[]

while True:
    #录入
    loop=True
    while loop:
        clock.tick()
        img = sensor.snapshot()
        objects = img.find_features(face_cascade, threshold=0.75, scale_factor=1.25)
        if objects:
            for r in objects:
                img.draw_rectangle(r,thickness=2)
            if button.right.state():
                img.to_grayscale()
                face = sensor.alloc_extra_fb(r[2],r[3], sensor.GRAYSCALE)
                face.draw_image(img,0,0,roi=r)
                face_size=[r[2],r[3]]
                print(face_size)
                loop=False
                break
        screen.display(img)
    loop=True
    #识别
    while loop:
        img = sensor.snapshot()
        objects = img.find_features(face_cascade, threshold=0.75, scale_factor=1.25)
        if objects:
            for r in objects:
                img_grayscale.draw_image(img,0,0,roi=r,x_scale=face_size[0]/r[2],y_scale=face_size[1]/r[3])
                img_grayscale.to_grayscale()
                results = img_grayscale.find_template(face, 0.60, search=image.SEARCH_EX,roi=(0,0,face_size[0],face_size[1])) #, roi=(10, 0, 60, 60))
                if results:
                    img.draw_rectangle(r,thickness=2,color=(255,0,0))
                else:
                    try:
                        img.draw_rectangle(r,thickness=2)
                    except:
                        pass
        if button.left.state():
            loop=False
        screen.display(img)
