'''
OpenMV4P_micro
屏幕驱动程序

因为修改了SPI端口，没能调用DMA三重缓冲，但最终对帧速影响不大。这点争取下个版本修复。
SPI速率调至最高了，如果屏幕显示异常，尝试降低速率至80Mhz以下。

备注：背光调节功能待更新

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20220906
'''
import lcd
from pyb import Pin,SPI
from pyb import ADC
from sensor import alloc_extra_fb,RGB565
class screen:
    def __init__(self):
        lcd.init(width=320,height=172)
        self.battery_measure= ADC("PA3")
        self.LCD_CS=Pin('PF6', Pin.OUT_PP)
        self.LCD_RST=Pin('PA1', Pin.OUT_PP)
        self.LCD_RS =Pin('PF10', Pin.OUT_PP)
        self.LCD_BACKLIGHT =Pin('PB5', Pin.OUT_PP)
        self.LCD_BACKLIGHT.high()
        self.img_display = alloc_extra_fb(320,172, RGB565)#光流所需的画布
        self.spi2=SPI(5,SPI.MASTER,baudrate=70*1000000,phase=1)    #SPI速率

        self.write_command(0x11)
        self.write_command(0x36,0x00)
        self.write_command(0x3A,0x05)

        self.write_c(0x21)
        self.write_c(0x11)

        self.write_c(0x36)
        self.write_d(0x60)
        self.set_resolution(0,34,320,172)

        self.write_command(0x29)

    def write_c(self,c):
        self.LCD_CS.low()
        self.LCD_RS.low()
        self.spi2.send(c)
        self.LCD_CS.high()

    def write_d(self,c):
        self.LCD_CS.low()
        self.LCD_RS.high()
        self.spi2.send(c)
        self.LCD_CS.high()

    def write_command(self,c,*data):
        self.write_c(c)
        if data:
            for d in data:
                self.write_d(d)

    def set_resolution(self,x,y,w,h):
        self.write_c(0x2a)#设置屏幕长宽像素
        self.write_d(int(x/256))
        self.write_d(x%256)
        self.write_d(int((x+w-1)/256))
        self.write_d((x+w-1)%256)
        self.write_c(0x2b)
        self.write_d(int(y/256))
        self.write_d(y%256)
        self.write_d(int((y+h-1)/256))
        self.write_d((y+h-1)%256)
        self.write_c(0x2C)

    def draw_battery_icon(self,img):
        self.battery_voltage=(self.battery_measure.read()*6.6)/4096
        #print(self.battery_voltage)
        img.draw_rectangle(275,14,30,13,color=(0xA6,0xA6,0xA6),fill=True)
        img.draw_rectangle(305,17,2,7,color=(0xA6,0xA6,0xA6))
        if self.battery_voltage>3.3:
            img.draw_rectangle(277,16,5,9,fill=True)
        if self.battery_voltage>3.5:
            img.draw_rectangle(284,16,5,9,fill=True)
        if self.battery_voltage>3.75:
            img.draw_rectangle(291,16,5,9,fill=True)
        if self.battery_voltage>3.9:
            img.draw_rectangle(298,16,5,9,fill=True)

    def display(self,img,battery_icon=True,x_size=None,y_size=None):
        if x_size or y_size:
            self.img_display.draw_image(img,0,0,x_size=x_size,y_size=y_size)
            if battery_icon:
                self.draw_battery_icon(self.img_display)
            lcd.display(self.img_display)
        else:
            if battery_icon:
                self.draw_battery_icon(img)
            lcd.display(img)


