'''
OpenMV4P_micro
按键驱动程序

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20220906
'''
from pyb import Pin

left_pin='PH3'
right_pin='PH2'

class button_state(Pin):
    def state(self):
        return not(self.value())

left = button_state(left_pin, Pin.IN, Pin.PULL_UP)
right = button_state(right_pin, Pin.IN, Pin.PULL_UP)


