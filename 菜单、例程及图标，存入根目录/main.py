'''
OpenMV4P_micro
主菜单程序

说明：
存入根目录的程序，会自动在主界面刷新出来。同时会加载，位于ICONS文件夹中的同名BMP图片，作为图标。
按住左键，通过倾斜移动画面，选取所需的程序。注意这里使用光流技术，需要摄像头面对的画面不能是单一纯色。
双击左键，可以拍照，修改当前选中的图标。在此界面中按左键退出，按右键确认。
单机右键，进入选中的程序。如果程序有错误，会在主界面报错。
在任何界面下（连接电脑端编程软件除外），按住左键，再按右键，设备重启，返回主页。

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20220906
'''
import sensor, image, time,screen,button
import pyb
from time import sleep
from os import listdir
from math import floor,ceil
from gc import collect

r=15    #图标切圆角的倒角半径

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.B64X32)

screen=screen.screen()  #声明屏幕
#一、扫描根目录下的py文件
py_file_names=listdir('')#扫描根目录下程序
py_file_names.sort()#文件排序
py_file_quantity=0
while py_file_quantity < len(py_file_names)-1:#删除非.py文件
    for py_file_quantity in range(len(py_file_names)):
        if not (py_file_names[py_file_quantity][-3:]=='.py' or py_file_names[py_file_quantity][-3:]=='.PY') or py_file_names[py_file_quantity]=='main.py'\
        or py_file_names[py_file_quantity]=='screen.py' or py_file_names[py_file_quantity]=='button.py':
            del py_file_names[py_file_quantity]
            break

    #上一节程序，如果倒数第二个文件不是py，删除后不会遍历最后一个文件，下面再遍历一遍解决这个BUG
for py_file_quantity in range(len(py_file_names)):
    if not (py_file_names[py_file_quantity][-3:]=='.py' or py_file_names[py_file_quantity][-3:]=='.PY') or py_file_names[py_file_quantity]=='main.py'\
        or py_file_names[py_file_quantity]=='screen.py' or py_file_names[py_file_quantity]=='button.py':
        del py_file_names[py_file_quantity]
        break
for py_file_quantity in range(len(py_file_names)):#删除扩展名
    py_file_names[py_file_quantity]=py_file_names[py_file_quantity][:-3]
py_file_quantity=len(py_file_names)

#二、根据py文件名，在ICONS文件夹中，加载同名图标。图标会被拉伸至100*100尺寸，切为圆角矩形。同时在图标中绘制文件名。
#注：图标需小于500k，24位bmp格式。推荐100*100。photoshop保存的bmp可能无法识别，建议使用其他格式，再用windows自带的画图保存为bmp。
#注：如果没有图标，或图标不符合上述要求，会加载失败。加载失败时会加载unknow_program.bmp。如果没有它，则加载一个纯白图标。
icon_mask=sensor.alloc_extra_fb(100,125,sensor.RGB565)#用于切圆角矩形的掩码
for n in range(py_file_quantity):
    #绘制图标
    exec('icon'+str(n)+'=sensor.alloc_extra_fb(100,125,sensor.RGB565)')
    try:
        exec("icon"+str(n)+".draw_image(image.Image('/ICONS/"+py_file_names[n]+".bmp',copy_to_fb=True),0,0,x_size=100,y_size=100)")
    except:
        try:
            exec("icon"+str(n)+".draw_image(image.Image('/ICONS/unknow_program.bmp',copy_to_fb=True),0,0,x_size=100,y_size=100)")
        except:
            exec('icon'+str(n)+'.draw_rectangle(0,0,100,100,fill=True)')
    #切圆角
    icon_mask.draw_circle(r,r,r,fill=True)
    icon_mask.draw_circle(100-r,r,r,fill=True)
    icon_mask.draw_circle(r,100-r,r,fill=True)
    icon_mask.draw_circle(100-r,100-r,r,fill=True)
    icon_mask.draw_rectangle(r,0,100-r*2,100,fill=True)
    icon_mask.draw_rectangle(0,r,100,100-r*2,fill=True)
    icon_mask.draw_rectangle(0,101,100,25,fill=True)
    exec('icon'+str(n)+'.b_and(icon_mask)')
    #绘制文件名
    if len(py_file_names[n])<10:
        exec('icon'+str(n)+'.draw_string(round((10-len(py_file_names[n]))*5),100,py_file_names[n],scale=2,x_spacing=1,mono_space=False)')
    elif len(py_file_names[n])>=10:
        exec('icon'+str(n)+'.draw_string(0,100,py_file_names[n][:9]+"..",scale=2,x_spacing=1,mono_space=False)')

#三、绘制图标部分
img_homepage = sensor.alloc_extra_fb(340,ceil(py_file_quantity/3)*125,sensor.RGB565) #完整的主页画布

#在画布上绘制图标，输入图标（程序）序号、尺寸（0~100）
draw_icon_number=0  #下面函数所需的序号和尺寸。因为exec对于执行局部变量报错，所以它所需的变量改为全局变量。
draw_icon_size=90
def draw_icon(n,s,erase=True):
    global py_file_names,img_homepage,r,number,size
    number=n
    size=s
    if size>100:size=100
    elif size<10:size=10
    r=round(r*size/100)
    try:
        if erase:
            img_homepage.draw_rectangle((number%3)*120,floor(number/3)*125,100,125,color=(0,0,0),fill=True)
        exec('img_homepage.draw_image(icon'+str(number)+',(number%3)*120+round((100-size)/2),floor(number/3)*125+round((100-size)/2),x_size=size,y_size=size+25)')
    except:
        pass

#绘制所有图标到画布，未选中的图标尺寸85
for number in range(py_file_quantity):
    draw_icon(number,85)

draw_icon(1,100)#初始状态选中1，尺寸100

img_display = sensor.alloc_extra_fb(320,172,sensor.RGB565) #显示所需的画布

#四、其他函数和变量
def extint_right(n):#负责重启的中断，占用右键中断。先按住左，再按右执行重启（返回桌面）。
    global loop
    if button.left.state():
        pyb.hard_reset()

extint1 = pyb.ExtInt(button.right_pin,pyb.ExtInt.IRQ_FALLING,pyb.Pin.PULL_UP,extint_right)

extra_fb = sensor.alloc_extra_fb(sensor.width(), sensor.height(), sensor.RGB565)#光流所需的画布
extra_fb.replace(sensor.snapshot())
cursor_x=-10    #光标初始坐标
cursor_y=30
homepage_length=floor(py_file_quantity/3)*125-30#主页长度
select=1    #主页当前所选的程序序号
last_select=1   #上一次选择的程序序号，用于恢复上一个程序图标的尺寸
img_display.draw_image(img_homepage,round(cursor_x),round(cursor_y))#用于显示的画布
screen.display(img_display)#进行一次初始显示。在循环中，没有需要刷新的内容时，不进行显示。
double_click_timer=0 #用于判断双击的时间存储
first_time_clear=True   #记录第一次非光流状态，用以清空画面
first_time_move=True    #记录第一次移动状态，用以唤醒摄像头（非移动状态摄像头休眠）
img = sensor.snapshot() #进行一次光流，以创建img
displacement = extra_fb.find_displacement(img)
extra_fb.replace(img)
#五、主循环
while True:
    #按下左键先判断
    if button.left.state():
        if first_time_move:
            sensor.sleep(False)
            first_time_clear=True
        first_time_move=False
        img = sensor.snapshot()#光流部分程序
        displacement = extra_fb.find_displacement(img)
        extra_fb.replace(img)
        if pyb.millis()-double_click_timer<600:#双击进入拍照模式
            while button.left.state():pass#等待按键抬起
            sensor.set_framesize(sensor.QQVGA)
            sensor.set_windowing(100,100)
            sensor.set_vflip(True)
            sensor.set_hmirror(True)
            print(str(cursor_x)+','+str(cursor_y))
            #拍照模式的循环，显示预览图像和提示字符
            while True:
                img = sensor.snapshot()
                img_homepage.draw_image(img,(select%3)*120,floor(select/3)*125,x_size=100)
                img_display.draw_image(img_homepage,round(cursor_x),round(cursor_y))
                img_display.draw_string(10,5,'Icon shooting',color=(255,0,0),scale=2,x_spacing=1,mono_space=False)
                img_display.draw_string(180,5,'back',color=(255,255,0),scale=2,x_spacing=1,mono_space=False)
                img_display.draw_rectangle(175,5,43,20,color=(255,255,0))
                img_display.draw_string(230,5,'enter',color=(255,255,0),scale=2,x_spacing=1,mono_space=False)
                img_display.draw_rectangle(225,5,50,20,color=(255,255,0))
                screen.display(img_display)
                if button.left.state(): #按左键退出
                    break
                if button.right.state():    #按右键保存并重启
                    img.save('/ICONS/'+py_file_names[select]+'.bmp')
                    pyb.hard_reset()
            sensor.set_framesize(sensor.B64X32) #退出后恢复分辨率，刷新显示
            sensor.set_vflip(False)
            sensor.set_hmirror(False)
            draw_icon(select,100)
            img_display.clear() #显示新内容
            img_display.draw_image(img_homepage,round(cursor_x),round(cursor_y))
            screen.display(img_display)
            double_click_timer=pyb.millis()-600
        else:   #左键长按状态
            double_click_timer=pyb.millis()
            while button.left.state():
                img = sensor.snapshot()
                #光流部分程序
                displacement = extra_fb.find_displacement(img)
                extra_fb.replace(img)
                if displacement.response() > 0.1:
                    cursor_x += displacement.x_translation() * 10   #调节光流速度比例
                    cursor_y -= displacement.y_translation() * 10
                    if cursor_x<-120:   #光标坐标限幅
                        cursor_x=-120
                    if cursor_x>120:
                        cursor_x=120
                    if cursor_y>36:
                        cursor_y=36
                    if cursor_y<-homepage_length:
                        cursor_y=-homepage_length
                    select=floor((86-cursor_y)/125)*3+floor((170-cursor_x)/120)#通过光标坐标计算当前选中的程序序号
                    if select!=last_select: #如果光标改变，则重新绘制选中的和上一个被选中的图标
                        draw_icon(select,100)
                        draw_icon(last_select,85)
                        last_select=select
                    img_display.clear() #显示新内容
                    img_display.draw_image(img_homepage,round(cursor_x),round(cursor_y))
                    screen.display(img_display)
    elif button.right.state():#如果右键按下
        sleep(0.02) #按键消抖
        if button.right.state():
            for n in range(0,25,5):#光标被选中后的缩小动画
                draw_icon(select,100-n)
                img_display.clear()
                img_display.draw_image(img_homepage,round(cursor_x),round(cursor_y))
                screen.display(img_display)
            while button.right.state():#等待右键抬起
                pass
            try:    #打开所选中的程序
                collect()
                __import__('/'+str(py_file_names[select]))
                print('/'+str(py_file_names[select]))
            except Exception as err:    #报错则在屏幕显示报错内容
                img_display.draw_rectangle(0,76,320,20,color=(150,150,150),fill=True)
                img_display.draw_string(2,76,'ERR:'+str(err),color=(255,0,0),scale=2,x_spacing=1,mono_space=False)
                screen.display(img_display)
                while not button.left.state():#按下左键后恢复图标尺寸
                    pass
                draw_icon(select,85)
                img_display.clear()
                img_display.draw_image(img_homepage,round(cursor_x),round(cursor_y))
                screen.display(img_display)
    else:
        if first_time_clear:
            img.clear()
            extra_fb.clear()
            sensor.sleep(True)
            first_time_move=True
        first_time_clear=False
