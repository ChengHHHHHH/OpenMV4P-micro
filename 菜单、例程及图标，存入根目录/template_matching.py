'''
OpenMV4P_micro
模板匹配

本程序采用模板匹配+实时更新。可以在目标发生简单变动后依然识别，但长时间运行有不稳定因素。
云台追踪中，也使用类似策略，但需要更详细纠错逻辑。

使用说明：
按右键录入，按左键取消录入。

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20220909
'''
import sensor, image, time,screen,button
from os import mkdir

sensor.reset()
sensor.set_framesize(sensor.QQVGA)
sensor.set_windowing(160,86)
sensor.set_pixformat(sensor.RGB565)
sensor.set_vflip(True)
sensor.set_hmirror(True)

clock = time.clock()

screen=screen.screen()

face_cascade = image.HaarCascade("frontalface", stages=25)

img_grayscale = sensor.alloc_extra_fb(160,86, sensor.GRAYSCALE)
image_data = sensor.alloc_extra_fb(50,50, sensor.GRAYSCALE)
face_size=[]

while True:
    #录入
    while True:
        clock.tick()
        img = sensor.snapshot()
        if button.right.state():
            image_data.draw_image(img,0,0,roi=(55,18,50,50))
            break
        img.draw_rectangle(55,18,50,50,thickness=2)
        screen.display(img,x_size=320)
    #识别
    while True:
        img = sensor.snapshot()
        img_grayscale.draw_image(img,0,0)
        img_grayscale.to_grayscale()
        results = img_grayscale.find_template(image_data, 0.70, step=2, search=image.SEARCH_EX)
        if results:
            print(results)
            image_data.draw_image(img_grayscale,0,0,roi=results)
            img.draw_rectangle(results,thickness=2,color=(255,0,0))
        if button.left.state():
            break
        screen.display(img,x_size=320)
