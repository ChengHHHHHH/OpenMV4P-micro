'''
OpenMV4P_micro
二维码识别

因为扩大画面，所以帧速略低，但识别率还可以。识别完成后显示字符，暂不支持中文（下一版本更新可支持）。

操作说明：
识别后画面静止，按右键继续识别。

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20220909
'''
import sensor, image, time,screen,button

sensor.reset()
sensor.set_framesize(sensor.HD)
sensor.set_windowing(1280,688)
sensor.set_pixformat(sensor.GRAYSCALE)
sensor.set_contrast(3)
sensor.set_vflip(True)
sensor.set_hmirror(True)

clock = time.clock()

screen=screen.screen()

while True:
    img = sensor.snapshot()
    codes = img.find_qrcodes()
    print(codes)
    if codes:
        for code in codes:
            if len(code[4])>35:
                img.draw_rectangle(0,280,1280,170,color=(150,150,150),fill=True)
                img.draw_string(60,280,code[4][:35]+'\r\n'+code[4][35:],scale=8,x_spacing=1,mono_space=False)
            else:
                img.draw_rectangle(0,300,1280,80,color=(150,150,150),fill=True)
                img.draw_string(60,300,code[4],scale=8,x_spacing=1,mono_space=False)
            screen.display(img,x_size=320)
            while not button.right.state():
                pass
    screen.display(img,x_size=320)
